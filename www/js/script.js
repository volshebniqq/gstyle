$(document).ready(function () {
    document.getElementById("msg-button").onclick = function () {
        $("#notifications-pop-up").fadeIn(400);
        $("#close-right-menu-pop-up").fadeIn(1);
        $(".messages-number").animate({"opacity": "0"}, 100);
        $(".exclamation-point").animate({"opacity": "1"}, 100);
        $(".fa-comment").animate({"color": "#aac3e6"}, 100);
    }
    document.getElementById("questions-button").onclick = function () {
        $("#questions").fadeIn(400);
        $("#close-right-menu-pop-up").fadeIn(1);
    }
    document.getElementById("notifications-button").onclick = function () {
        $("#bell-pop-up").fadeIn(400);
        $("#close-right-menu-pop-up").fadeIn(1);
    }
    document.getElementById("navbar-right-menu-button").onclick = function () {
        $("#navbar-right-menu").fadeIn(400);
        $("#close-right-menu-pop-up").fadeIn(1);
    }
    document.getElementById("profile-button").onclick = function () {
        $("#profile-pop-up").fadeIn(400);
        $("#profile-button").animate({'opacity': '0.5'}, 500);
        $("#close-right-menu-pop-up").fadeIn(1);
    }
    document.getElementById("close-right-menu-pop-up").onclick = function () {
        $(".right-menu-pop-up").fadeOut(400);
        $(".navbar-right").children().animate({'opacity': '1'}, 500);
        $("#close-right-menu-pop-up").fadeOut(1);
        $(".messages-number").animate({"opacity": "1"}, 100);
        $(".exclamation-point").animate({"opacity": "0"}, 100);
        $(".fa-comment").animate({"color": "#f71a2f"}, 100);
    }
    $("#close-notifications").click(function () {
        $("#notifications-pop-up").fadeOut(400);
        $("#close-right-menu-pop-up").fadeOut(1);
        $(".messages-number").animate({"opacity": "1"}, 100);
        $(".exclamation-point").animate({"opacity": "0"}, 100);
        $(".fa-comment").animate({"color": "#f71a2f"}, 100);
    });
    var menu = true;
    var messages = true;
    $(".top-menu-button").click(function () {
        if (menu == true) {
            $(".sidebar").animate({'margin-left': '-16.6666%'}, 500);
            $(".sidebar-toggle-button").animate({'right': '-20px'}, 500);
            menu = false;
            $("#wrapper").delay(100).animate({'margin-left': '0'}, 100);
            $("#sidebar-toggle-button-inner").removeClass("fa-chevron-left");
            $("#sidebar-toggle-button-inner").addClass("fa-chevron-right");
        } else {
            $(".sidebar").animate({'margin-left': '0'}, 500);
            $(".sidebar-toggle-button").animate({'right': '-13px'}, 500);
            menu = true;
            $("#wrapper").animate({'margin-left': '16.66%'}, 100);
            $("#sidebar-toggle-button-inner").removeClass("fa-chevron-right");
            $("#sidebar-toggle-button-inner").addClass("fa-chevron-left");
        }
    });
    $(".sidebar-toggle-button").click(function () {
        if (menu == true) {
            $(".sidebar").animate({'margin-left': '-16.6666%'}, 500);
            $(".sidebar-toggle-button").animate({'right': '-20px'}, 500);
            menu = false;
            $("#wrapper").delay(100).animate({'margin-left': '0'}, 100);
            $("#sidebar-toggle-button-inner").removeClass("fa-chevron-left");
            $("#sidebar-toggle-button-inner").addClass("fa-chevron-right");
        } else {
            $(".sidebar").animate({'margin-left': '0'}, 500);
            $(".sidebar-toggle-button").animate({'right': '-13px'}, 500);
            menu = true;
            $("#wrapper").animate({'margin-left': '16.66%'}, 100);
            $("#sidebar-toggle-button-inner").removeClass("fa-chevron-right");
            $("#sidebar-toggle-button-inner").addClass("fa-chevron-left");
        }
    });
    $("#close-messages").click(function () {
        document.getElementById("messages-panel").style.display = "none";
        $("#transactions-panel-container").removeClass("col-md-8");
        $("#transactions-panel-container").addClass("col-md-12");
    });
    $("#hide-messages").click(function () {
        if (messages == true) {
            $(".messages-panel-list").slideUp(500);
            $("#hide-messages").removeClass("glyphicon-chevron-up");
            $("#hide-messages").addClass("glyphicon-chevron-down");
            messages = false;
        } else {
            $(".messages-panel-list").slideDown(500);
            $("#hide-messages").removeClass("glyphicon-chevron-down");
            $("#hide-messages").addClass("glyphicon-chevron-up");
            messages = true;
        }
    });
});
